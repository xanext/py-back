import logging
from logging.config import dictConfig

from flask import (
    Flask,
    Blueprint,
)

import settings
import views
from utils.utils import error_handler, escape_query_characters_middleware

log = logging.getLogger(__name__)

app = Flask(__name__)


def create_app():
    log.info('creating app')

    app = Flask(__name__)
    app.add_url_rule('/ping/',
                     view_func=views.ping, methods=['GET'])

    api_v1 = Blueprint('1', __name__)
    # Авторизация, регистрация и получение данных о пользователе
    api_v1.add_url_rule('/oauth/',
                        view_func=views.OAuth.as_view(), methods=['POST'])
    api_v1.add_url_rule('/registration/',
                        view_func=views.Registration.as_view(), methods=['POST'])
    api_v1.add_url_rule('/get-account-info/',
                        view_func=views.GetAccount.as_view(), methods=['GET'])
    # ИВЕНТЫ
    api_v1.add_url_rule('/api/events/add-event/',
                        view_func=views.AddEvent.as_view(), methods=['GET', 'POST'])
    api_v1.add_url_rule('/api/events/get-all-events/',
                        view_func=views.GetAllEvents.as_view(), methods=['GET'])
    api_v1.add_url_rule('/api/events/get-events/',
                        view_func=views.GetEvents.as_view(), methods=['GET'])
    api_v1.add_url_rule('/api/events/get-events-near-place/',
                        view_func=views.GetEventsNearPlace.as_view(), methods=['GET'])
    api_v1.add_url_rule('/api/events/get-events-for-point/',
                        view_func=views.GetEventsForPoint.as_view(), methods=['GET'])
    api_v1.add_url_rule('/api/events/get-events-with-title/',
                        view_func=views.GetEventsWithTitle.as_view(), methods=['GET'])
    api_v1.add_url_rule('/api/events/get-events-with-category/',
                        view_func=views.GetEventsWithCategory.as_view(), methods=['GET'])
    api_v1.add_url_rule('/api/events/get-event-by-uid/',
                        view_func=views.GetEventsByUid.as_view(), methods=['GET'])
    # ТОЧКИ
    api_v1.add_url_rule('/api/points/add-point/',
                        view_func=views.AddPoint.as_view(), methods=['GET', 'POST'])
    api_v1.add_url_rule('/api/points/get-all-points/',
                        view_func=views.GetAllPoints.as_view(), methods=['GET'])
    api_v1.add_url_rule('/api/points/get-points-near-place/',
                        view_func=views.GetPointsNearPlace.as_view(), methods=['GET'])
    api_v1.add_url_rule('/api/points/get-points-with-category/',
                        view_func=views.GetPointsWithCategory.as_view(), methods=['GET'])
    api_v1.add_url_rule('/api/points/get-point-by-id/',
                        view_func=views.GetPointById.as_view(), methods=['GET'])
    # НОВОСТИ
    api_v1.add_url_rule('/api/news/get-news/',
                        view_func=views.GetLastNews.as_view(), methods=['GET'])
    api_v1.add_url_rule('/api/track/add-track/',
                        view_func=views.AddTrack.as_view(), methods=['POST'])
    api_v1.add_url_rule('/api/track/get-tracks-ids/',
                        view_func=views.GetTracksIds.as_view(), methods=['GET'])
    api_v1.add_url_rule('/api/track/get-track/',
                        view_func=views.GetTrack.as_view(), methods=['GET'])
    app.register_blueprint(api_v1, url_prefix='/1')
    app.errorhandler(Exception)(error_handler)

    app.wsgi_app = escape_query_characters_middleware(app.wsgi_app)
    return app


def prepare_environment():
    dictConfig(settings.LOGGING)


def execute_app():
    prepare_environment()

    application = create_app()

    return application
