# PY-BACK
REST API для экспресс проекта для HackUniversity (https://www.hackuniversity.ru/)

##  Список всех доступных ручек (DEPRECATED).   
       
* [x]  `/ping/` **GET** .  
*ACCOUNT*
* [x]  `/oauth/` **POST**
* [x]  `/registration/` **POST**
* [x]  `/get-account-info/` **GET** .   
*POINTS*
* [x]  `/api/points/add-point/` **GET POST**
* [x]  `/api/points/get-all-points/` **GET**  
* [x]  `/api/points/get-points-near-place/` **GET** 
* [x]  `/api/points/get-points-with-category/` **GET**    
*EVENTS*
* [x]  `/api/events/get-all-events/` **GET**
* [x]  `/api/events/get-events/` **GET**
* [x]  `/api/events/get-events-near-place/` **GET**
* [x]  `/api/events/get-events-for-point/` **GET**
* [x]  `/api/events/get-events-with-title/` **GET**
* [x]  `/api/events/get-events-with-category/` **GET**


###  **Авторизация** .           
PATH: **POST /oauth/** .    
Входные параметры: *login*, *password* .  
Возвращаемое значение: 

    {
       "access_token": "AIODS6eRc8csFamUCl6bzxEh7T-95FWpg-yqdFVMlosI1KGlpxhv--hIhS-A",        
       "account": { 
           "age": 20,         
           "firstname": "Alexey",    
           "lastname": "Romanov",     
           "login": "egorius",     
           "uid": 891554259334   
       }, 
       "status": "ok" 
    }
    
###  **Регистрация** .             
PATH: **POST /registration/** .      
Входные параметры: *login*, *password*, *lastname*, *firstname*, *age*.     
Возвращаемое значение: 

    "account": {
        "age": 20,
        "firstname": "Alexey",
        "lastname": "Romanov",
        "login": "sergrrr",
        "uid": 0
    },
    "msg": "Registration success",
    "status": "ok"


###  **Информация об аккаунте по uid** .  
HEADER: `Authorization: OAuth {token}` .              
PATH: **GET /get-account-info/** .      
Пример запроса: `http://127.0.0.1:8090/1/get-account-info/?uid=891554259334`     
Возвращаемое значение: 

    {
       "account": {
           "age": 20,
           "firstname": "Alexey",
           "lastname": "Romanov",
           "login": "sergrrr",
       },
       "status": "ok"
    }

###  **Добавление новой точки** .  
HEADER: `Authorization: OAuth {token}` .                
PATH: **GET POST /api/points/add-point/** .      
Пример запроса: `http://127.0.0.1:8090/1/api/points/add-point/?point_x=1.213&point_y=34543543.3454353&point_z=23432&category=Спортивная площадка`       
Возвращаемое значение: 

    {
      "msg": "Point successfully added",
      "point_id": 3,
      "status": "ok"
    }
    
###  **Добавление нового ивента** .  
HEADER: `Authorization: OAuth {token}` .                
PATH: **GET POST /api/events/add-event/** .      
Пример запроса: `http://127.0.0.1:8090/1/api/events/add-event/?title=Серега &description=Супер&date=2020-03-21 21:15:37&author_uid=891554259334&point_id=1`       
Возвращаемое значение: 

    {
       "event_id": 7,
       "msg": "Event successfully added",
       "status": "ok"
    }
    
###  **Список всех ивентов** .  
HEADER: `Authorization: OAuth {token}` .                
PATH: **GET /api/events/get-all-events/** .             
Возвращаемое значение: 

    [
       {
          'event_id': '123213',
          'title': 'Соревнования по пинанию хуев',
          'description': '',
          'point': {
              'point_id': 234234
              'category': 'Турник'
              'point_x': x,
              'point_y': y,
              'point_z': z,
          },
          'date': '2020-03-21 11:08:37',
          'author_uid': '23748327483274',
    },
    ...
    ],
    "status": "ok"

###  **Список всех ивентов по страницам** .  
HEADER: `Authorization: OAuth {token}` .                
PATH: **GET . /api/events/get-events/** .             
Возвращаемое значение: 

    [
       {
          'event_id': '123213',
          'title': 'Соревнования по пинанию хуев',
          'description': '',
          'point': {
              'point_id': 234234
              'category': 'Турник'
              'point_x': x,
              'point_y': y,
              'point_z': z,
          },
          'date': '2020-03-21 11:08:37',
          'author_uid': '23748327483274',
    },
    ...
    ],
    "link": "/1/get_events/?offset=100",
    "status": "ok"

###  **Список ивентов рядом с точкой** .  
HEADER: `Authorization: OAuth {token}` .                
PATH: **GET  /api/events/get-events-near-place/** .      
Пример запроса: `http://127.0.0.1:8090/1/api/events/get-events-near-place/?point_x=700&point_y=34543543&scale=1200`       
Возвращаемое значение: 

    [
       {
          'event_id': '123213',
          'title': 'Соревнования по пинанию хуев',
          'description': '',
          'point': {
              'point_id': 234234
              'category': 'Турник'
              'point_x': x,
              'point_y': y,
              'point_z': z,
          },
          'date': '2020-03-21 11:08:37',
          'author_uid': '23748327483274',
    },
    ...
    ],
    "status": "ok"

###  **Список ивентов для заданной точки (места)** .  
HEADER: `Authorization: OAuth {token}` .                
PATH: **GET  /api/events/get-events-for-point/** .      
Пример запроса: `http://127.0.0.1:8090/1/api/events/get-events-for-point/?point_id=2`       
Возвращаемое значение: 

    [
       {
          'event_id': '123213',
          'title': 'Соревнования по пинанию хуев',
          'description': '',
          'point': {
              'point_id': 2
              'category': 'Турник'
              'point_x': x,
              'point_y': y,
              'point_z': z,
          },
          'date': '2020-03-21 11:08:37',
          'author_uid': '23748327483274',
    },
    ...
    ],
    "status": "ok"

###  **Список ивентов с определенным названием** .  
HEADER: `Authorization: OAuth {token}` .                
PATH: **GET /api/events/get-events-with-title/** .      
Пример запроса: `http://127.0.0.1:8090/1/api/events/get-events-with-title/?title=Серега`       
Возвращаемое значение: 

    [
       {
          'event_id': '123213',
          'title': 'Серега',
          'description': 'Серега-лох',
          'point': {
              'point_id': 2
              'category': 'Турник'
              'point_x': x,
              'point_y': y,
              'point_z': z,
          },
          'date': '2020-03-21 11:08:37',
          'author_uid': '23748327483274',
    },
    ...
    ],
    "status": "ok"

###  **Список ивентов с определенной категорией места** .  
HEADER: `Authorization: OAuth {token}` .                
PATH: **GET   /api/events/get-events-with-category/** .      
Пример запроса: `http://127.0.0.1:8090/1/api/events/get-events-with-category/?category=Турник`       
Возвращаемое значение: 

    [
       {
          'event_id': '123213',
          'title': 'Серега',
          'description': 'Серега-лох',
          'point': {
              'point_id': 2
              'category': 'Турник'
              'point_x': x,
              'point_y': y,
              'point_z': z,
          },
          'date': '2020-03-21 11:08:37',
          'author_uid': '23748327483274',
    },
    ...
    ],
    "status": "ok"

###  **Список точек рядом с переданными координатами** .  
HEADER: `Authorization: OAuth {token}` .                
PATH: **GET  /api/points/get-points-near-place/** .      
Пример запроса: `http://127.0.0.1:8090/1/api/points/get-points-near-place/?point_x=1100&point_y=34543543&scale=400`       
Возвращаемое значение: 

    {
    "points": [
        {
            "category": "Турник",
            "point_id": 1,
            "point_x": "1222.21000000",
            "point_y": "34543543.34543539",
            "point_z": "234.32600000"
        },
        {
            "category": "Парк",
            "point_id": 2,
            "point_x": "1222.21000000",
            "point_y": "34543543.34543539",
            "point_z": "234.32600000"
        }, 
        ...
    ],
    "status": "ok"
    }

###  **Список мест с определенной категорией** .  
HEADER: `Authorization: OAuth {token}` .                
PATH: **GET   /api/events/get-events-with-category/** .      
Пример запроса: `http://127.0.0.1:8090/1/api/points/get-points-with-category/?category=Турник`       
Возвращаемое значение: 

    {
    "points": [
        {
            "category": "Турник",
            "point_id": 1,
            "point_x": "1222.21000000",
            "point_y": "34543543.34543539",
            "point_z": "234.32600000"
        },
        ...
    ],
    "status": "ok"
    }

###  **Список всех мест** .  
HEADER: `Authorization: OAuth {token}` .                
PATH: **GET /api/points/get-all-points/** .             
Возвращаемое значение: 

    {
    "points": [
        {
            "category": "Турник",
            "point_id": 1,
            "point_x": "1222.21000000",
            "point_y": "34543543.34543539",
            "point_z": "234.32600000"
        },
        ...
    ],
    "status": "ok"
    }
