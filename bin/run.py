from app import execute_app


def run_app():
    app = execute_app()
    app.run(host= '0.0.0.0', port=8090)


if __name__ == '__main__':
    run_app()
