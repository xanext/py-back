class XanextServiceException(Exception):

    def __init__(self, error_description):
        self.error_description = error_description


class TemporarilyUnavailableError(XanextServiceException):
    code = 'backend.temporary'


class InvalidRequestError(XanextServiceException):
    code = 'request.invalid'


class HeadersMissedError(XanextServiceException):
    code = 'headers.missed'


class AuthorizationError(XanextServiceException):
    code = 'authorization.error'


class RegistrationError(XanextServiceException):
    code = 'registration.exception'


class AuthorizationHeaderError(XanextServiceException):
    code = 'authorization_header.invalid'


class XanextInternalException(XanextServiceException):
    code = 'backend.permanent'


class ResponseParsingError(XanextServiceException):
    code = 'backend.failed'


class OAuthTokenInvalid(XanextServiceException):
    code = 'oauth_token.invalid'


class AddPointError(XanextServiceException):
    code = 'add_point.error'


class AddEventError(XanextServiceException):
    code = 'add_event.error'


class PointIdInvalid(XanextServiceException):
    code = 'point_id.invalid'
