# coding=utf-8
import json

from formencode.validators import (
    FancyValidator,
    Invalid,
)

from model.point import Point
from settings import point_categories


class CategoryValidator(FancyValidator):
    """Проверка запроса на наличие необходимых ключей"""

    @staticmethod
    def _to_python(value, state):
        category = value
        if category not in point_categories:
            raise Invalid('should be one of {}'.format(point_categories), value, state)
        return category