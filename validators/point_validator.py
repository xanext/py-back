# coding=utf-8
import json

from formencode.validators import (
    FancyValidator,
    Invalid,
)

from model.point import Point


class PointValidator(FancyValidator):
    """Проверка запроса на наличие необходимых ключей"""

    @staticmethod
    def _to_python(value, state):
        print(value)
        points = json.loads(value)
        return points