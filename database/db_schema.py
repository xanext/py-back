# -*- coding: utf-8 -*-
from sqlalchemy import ForeignKey, DateTime
from sqlalchemy.schema import (
    Column,
    MetaData,
    Table
)
from sqlalchemy.types import (
    Integer,
    BigInteger,
    String,
    Numeric
)

metadata = MetaData()

point_table = Table(
    'point_table',
    metadata,
    Column('point_id', BigInteger, primary_key=True, nullable=False, autoincrement=True),
    Column('name', String, nullable=False),
    Column('category', String, nullable=False),
    Column('point_x', Numeric, nullable=False),
    Column('point_y', Numeric, nullable=False),
    Column('point_z', Numeric, nullable=False),
)

account_table = Table(
    'account_table',
    metadata,
    Column('uid', BigInteger, primary_key=True, nullable=False),
    Column('login', String, nullable=False),
    Column('password', String, nullable=False),
    Column('firstname', String, nullable=False),
    Column('lastname', String, nullable=False),
    Column('age', Integer, nullable=False),
)

token_table = Table(
    'token_table',
    metadata,
    Column('token', String, primary_key=True, nullable=False),
    Column('uid', BigInteger, ForeignKey('account_table.uid', ondelete="CASCADE"), nullable=False),
)

event_table = Table(
    'event_table',
    metadata,
    Column('event_id', BigInteger, primary_key=True, nullable=False, autoincrement=True),
    Column('title', String, nullable=False),
    Column('description', String, nullable=False),
    Column('date', DateTime, nullable=False),
    Column('author_uid', BigInteger, ForeignKey('account_table.uid', ondelete="CASCADE"), nullable=False),
    Column('point_id', BigInteger, ForeignKey('point_table.point_id', ondelete="CASCADE"), nullable=False),
)

track_table = Table(
    'track_table',
    metadata,
    Column('track_id',     BigInteger, primary_key=True, nullable=False, autoincrement=True),
    Column('point_number', BigInteger, nullable=False, autoincrement=True),
    Column('point_id',     BigInteger, ForeignKey('point_table.point_id', ondelete="CASCADE"), nullable=False),
    Column('point_x',      Numeric, nullable=False),
    Column('point_y',      Numeric, nullable=False),
    Column('point_z',      Numeric, nullable=False),
)
