CREATE TABLE account_table
(
    uid          bigint      UNIQUE NOT NULL,
    login        varchar(60) UNIQUE NOT NULL,
    password     varchar(256) NOT NULL,
    firstname    varchar(60) NOT NULL,
    lastname     varchar(60) NOT NULL,
    age          int         NOT NULL,
    PRIMARY KEY (uid)
);

CREATE TABLE token_table
(
    token  varchar(60)  UNIQUE NOT NULL,
    uid    bigint       UNIQUE NOT NULL,
    FOREIGN KEY (uid) REFERENCES account_table (uid) ON DELETE CASCADE,
    PRIMARY KEY (token)
);

CREATE TABLE point_table
(
    point_id     serial,
    name         varchar(60) NOT NULL,
    category     varchar(60) NOT NULL,
    point_x      numeric(20, 8)   NOT NULL,
    point_y      numeric(20, 8)    NOT NULL,
    point_z      numeric(20, 8)    NOT NULL,
    PRIMARY KEY(point_id)
);

CREATE TABLE event_table
(
    event_id     bigserial     UNIQUE NOT NULL,
    title        varchar(60)   NOT NULL,
    description  varchar(1000) NOT NULL,
    date         timestamp     NOT NULL,
    author_uid   bigint        NOT NULL,
    point_id     bigint        NOT NULL,
    FOREIGN KEY (author_uid) REFERENCES account_table (uid) ON DELETE CASCADE,
    FOREIGN KEY (point_id)   REFERENCES point_table (point_id) ON DELETE CASCADE,
    PRIMARY KEY (event_id)
);

CREATE TABLE track_table
(
    track_id     bigint             NOT NULL,
    point_number bigserial          UNIQUE NOT NULL,
    point_id     bigint             NOT NULL,
    point_x      numeric(20, 8)     NOT NULL,
    point_y      numeric(20, 8)     NOT NULL,
    point_z      numeric(20, 8)     NOT NULL,
    FOREIGN KEY (point_id)   REFERENCES point_table (point_id) ON DELETE CASCADE,
    PRIMARY KEY(point_number)
);