import json
import logging
from datetime import datetime

from sqlalchemy import create_engine, text
from sqlalchemy.exc import (
    InterfaceError,
    OperationalError,
)
from sqlalchemy.sql import (
    delete,
    literal_column,
    select,
)

from exceptions import TemporarilyUnavailableError
from utils.utils import rename_exception_decorator, retry_decorator

log = logging.getLogger(__name__)


def _apply_whereclause(clause, **kwargs):
    for key in kwargs:
        clause = clause.where(literal_column(key) == kwargs[key])
    return clause


# noinspection SqlDialectInspection,SqlNoDataSourceInspection
class DB(object):
    def __init__(self, db_config):
        db_string = ('postgres+psycopg2://'
                     '%(user)s:%(password)s@%(host)s:%(port)s/%(db_name)s'
                     '?target_session_attrs=read-write' % db_config)
        self.engine = create_engine(db_string)

    @rename_exception_decorator((OperationalError, InterfaceError), TemporarilyUnavailableError)
    @retry_decorator(exception=(OperationalError, InterfaceError))
    def run_queries(self, *queries):
        """
        Query should be function with only one parameter - connection
        Function returns list with results for each query
        """
        result = []
        with self.engine.begin() as connection:
            for query in queries:
                result.append(query(connection))
        return result

    def make_add_query(self, table, **kwargs):
        """
        Inserts to table
        Returns dict with entity data
        """

        def _query(connection):
            log.debug('Inserting %s to %s' % (str(kwargs), table.name))
            connection.execute(table.insert().values(**kwargs))
            return kwargs

        return _query

    def make_remove_query(self, table, **kwargs):
        """
        Returns True if something was removed, False if nothing was removed
        """

        def _query(connection):
            rowcount = connection.execute(_apply_whereclause(delete(table), **kwargs)).rowcount
            log.debug('Removing %s from %s. Removed %d' % (str(kwargs), table.name, rowcount))
            return rowcount > 0

        return _query

    def add(self, table, **kwargs):
        return self.run_queries(self.make_add_query(table, **kwargs))[0]

    def remove(self, table, **kwargs):
        return self.run_queries(self.make_remove_query(table, **kwargs))[0]

    def get(self, table, **kwargs):
        """
        Returns list
        """
        return self.run_queries(lambda connection: [dict(row) for row in connection.execute(
            _apply_whereclause(select([table]), **kwargs)).fetchall()])[0]

    def get_all_events_query(self):
        expiration_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        sql = text('''
        SELECT name, event_table.event_id, event_table.date, event_table.title, event_table.description, 
        event_table.author_uid, point_table.point_id,  point_table.point_x, point_table.point_y, point_table.point_z, 
        point_table.category
        FROM event_table 
            JOIN point_table ON point_table.point_id = event_table.point_id
        WHERE event_table.date >= :expiration_time
         ''')

        def _query(connection):
            return connection.execute(sql, expiration_time=expiration_time).fetchall()

        return _query

    def get_events_with_offset_query(self, offset):
        expiration_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        sql = text('''
        SELECT name, event_table.event_id, event_table.date, event_table.title, event_table.description, 
        event_table.author_uid, point_table.point_id, point_table.point_x, point_table.point_y, point_table.point_z, 
        point_table.category
        FROM event_table 
            JOIN point_table ON point_table.point_id = event_table.point_id
        WHERE event_table.date >= :expiration_time
        LIMIT 100 OFFSET :offset;
         ''')

        def _query(connection):
            return connection.execute(sql, offset=offset, expiration_time=expiration_time).fetchall()

        return _query

    def get_events_near_point_query(self, point, scale):
        expiration_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        low_x = point.point_x - scale / 2
        high_x = point.point_x + scale / 2
        low_y = point.point_y - scale / 2
        high_y = point.point_y + scale / 2

        sql = text('''
        SELECT name, event_table.event_id, event_table.date, event_table.title, event_table.description, 
        event_table.author_uid, point_table.point_id, point_table.point_x , point_table.point_y, point_table.point_z, 
        point_table.category
        FROM event_table 
            JOIN point_table ON point_table.point_id = event_table.point_id
        WHERE point_table.point_x <= :high_x AND point_table.point_x >= :low_x 
        AND point_table.point_y <= :high_y AND point_table.point_y >= :low_y
        AND event_table.date >= :expiration_time;
        ''')

        def _query(connection):
            return connection.execute(
                sql, low_x=low_x, high_x=high_x, low_y=low_y, high_y=high_y, expiration_time=expiration_time
            ).fetchall()

        return _query

    def get_events_for_point_query(self, point_id):
        expiration_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        sql = text('''
        SELECT name, event_table.event_id, event_table.date, event_table.title, event_table.description, 
        event_table.author_uid, point_table.point_id, point_table.point_x, point_table.point_y, point_table.point_z, 
        point_table.category
        FROM event_table 
            JOIN point_table ON point_table.point_id = event_table.point_id
        WHERE event_table.date >= :expiration_time AND point_table.point_id = :point_id;
        ''')

        def _query(connection):
            return connection.execute(
                sql, point_id=point_id, expiration_time=expiration_time
            ).fetchall()

        return _query

    def get_events_with_category_query(self, category):
        expiration_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        sql = text('''
        SELECT name, event_table.event_id, event_table.date, event_table.title, event_table.description, 
        event_table.author_uid, point_table.point_id, point_table.point_x, point_table.point_y, point_table.point_z, 
        point_table.category
        FROM event_table 
            JOIN point_table ON point_table.point_id = event_table.point_id
        WHERE point_table.category = :category AND event_table.date >= :expiration_time;
         ''')

        def _query(connection):
            return connection.execute(sql, category=category, expiration_time=expiration_time).fetchall()

        return _query

    def get_events_by_uid_query(self, uid):
        expiration_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        sql = text('''
        SELECT name, event_table.event_id, event_table.date, event_table.title, event_table.description, 
        event_table.author_uid, point_table.point_id, point_table.point_x, point_table.point_y, point_table.point_z, 
        point_table.category
        FROM event_table 
            JOIN point_table ON point_table.point_id = event_table.point_id
        WHERE event_table.author_uid = :uid AND event_table.date >= :expiration_time;
         ''')

        def _query(connection):
            return connection.execute(sql, uid=uid, expiration_time=expiration_time).fetchall()

        return _query

    def get_events_with_title_query(self, title):
        expiration_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        sql = text('''
        SELECT name, event_table.event_id, event_table.date, event_table.title, event_table.description, 
        event_table.author_uid,point_table.point_id,  point_table.point_x, point_table.point_y, point_table.point_z, point_table.category
        FROM event_table 
            JOIN point_table ON point_table.point_id = event_table.point_id
        WHERE event_table.title = :title AND event_table.date >= :expiration_time;
         ''')

        def _query(connection):
            return connection.execute(sql, title=title, expiration_time=expiration_time).fetchall()

        return _query

    def get_events_in_last_10_query(self):
        expiration_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        sql = text('''
        SELECT name, event_table.event_id, event_table.date, event_table.title, event_table.description, 
        event_table.author_uid, point_table.point_id,  point_table.point_x, point_table.point_y, point_table.point_z, 
        point_table.category
        FROM event_table 
            JOIN point_table ON point_table.point_id = event_table.point_id
        WHERE event_table.date >= :expiration_time ORDER BY event_table.event_id DESC LIMIT 10
         ''')

        def _query(connection):
            return connection.execute(sql, expiration_time=expiration_time).fetchall()

        return _query

    def get_points_near_point_query(self, point, scale):
        low_x = point.point_x - scale / 2
        high_x = point.point_x + scale / 2
        low_y = point.point_y - scale / 2
        high_y = point.point_y + scale / 2

        sql = text('''
        SELECT name, point_id, point_x , point_y, point_z, category FROM point_table
        WHERE point_table.point_x <= :high_x AND point_table.point_x >= :low_x 
        AND point_table.point_y <= :high_y AND point_table.point_y >= :low_y;
        ''')

        def _query(connection):
            return connection.execute(
                sql, low_x=low_x, high_x=high_x, low_y=low_y, high_y=high_y
            ).fetchall()

        return _query

    def get_tracks_in_last_10_query(self):
        expiration_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        sql = text('''
        SELECT name, event_table.event_id, event_table.date, event_table.title, event_table.description, 
        event_table.author_uid, point_table.point_id,  point_table.point_x, point_table.point_y, point_table.point_z, 
        point_table.category
        FROM event_table 
            JOIN point_table ON point_table.point_id = event_table.point_id
        WHERE event_table.date >= :expiration_time ORDER BY event_table.event_id DESC LIMIT 10
         ''')

        def _query(connection):
            return connection.execute(sql, expiration_time=expiration_time).fetchall()

        return _query

    def get_track_by_point_id(self, point_id):
        sql = text('''
        SELECT DISTINCT track_table.track_id
        FROM track_table 
        WHERE track_table.point_id = :point_id;
         ''')

        def _query(connection):
            return connection.execute(sql, point_id=point_id).fetchall()

        return _query

    def get_track_by_track_id(self, track_id):
        sql = text('''
        SELECT track_table.point_id,  track_table.point_x, track_table.point_y, track_table.point_z,
        track_table.point_number
        FROM track_table
        WHERE track_table.track_id = :track_id ORDER BY track_table.point_number ASC;
         ''')

        def _query(connection):
            return connection.execute(sql, track_id=track_id).fetchall()

        return _query


_db = None


def get_db():
    global _db
    if _db is None:
        db_config = load_db_config()
        print(db_config)
        _db = DB(db_config)
    return _db


def load_db_config():
    with open('db.config') as db_json:
        return json.load(db_json)
