# coding=utf-8
import logging

from formencode import validators

from backend.oauth_backend import (
    register_account,
)
from model.account import Account
from views.base import Schema, BaseView

log = logging.getLogger(__name__)


class RegistrationForm(Schema):
    login = validators.String(not_empty=True, strip=True)
    password = validators.String(not_empty=True, strip=True)
    firstname = validators.String(not_empty=True, if_missinhg='default_firstname', strip=True)
    lastname = validators.String(not_empty=True, if_missinhg='default_lastname', strip=True)
    age = validators.Int(not_empty=True, if_missinhg=18,  strip=True)


class Registration(BaseView):
    form = RegistrationForm

    def process_request(self):
        account_data = {
            'login': self.form_values['login'],
            'password': self.form_values['password'],
            'firstname': self.form_values['firstname'],
            'lastname': self.form_values['lastname'],
            'age': self.form_values['age'],
            'uid': 0
        }
        account = Account(**account_data)

        register_account(account)

        del account_data['password']
        self.response_values = {
            'status': 'ok',
            'account': account_data,
            'msg': 'Registration success',
        }


__all__ = (
    'Registration',
)
