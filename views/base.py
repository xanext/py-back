# -*- coding: utf-8 -*-
import logging

from flask import request
from flask.views import View
from formencode import (
    Invalid,
    Schema,
)

from exceptions import (
    InvalidRequestError,
    HeadersMissedError,
)
from utils.utils import (
    error_handler,
    ok_response
)

log = logging.getLogger(__name__)


class Schema(Schema):
    allow_extra_fields = True
    filter_extra_fields = True


class BaseView(View):
    form = Schema

    @property
    def all_values(self):
        values = request.args.to_dict()
        values.update(request.form.to_dict())
        values.update(self.path_values)
        return values

    @property
    def headers(self):
        return request.headers

    # Список необходимых хедеров
    expected_headers = []

    balancer_headers = []

    def __init__(self):
        self.validators = []
        self.response_values = {}
        self.form_values = {}

    def dispatch_request(self, **kwargs):
        self.path_values = kwargs
        try:
            log.info('Called \'%s\' with args {%s}' % (
                self.__class__,
                ', '.join(['\'%s\': \'%s\'' % (k, v)
                           for k, v in self.all_values.items()]))
                     )
            self.process_form(self.form(), self.all_values)
            self.check_no_repeated_params()
            self.check_headers()
            print('---------START----------')
            print('Вот, что нам пришло', self.form_values)
            self.process_request()
            print('Вот, что мы отдали', self.response_values)
            print('--------FINISH----------')
        except Exception as e:
            return self.respond_error(e)

        return self.respond_success()

    def respond_success(self):
        return ok_response(**self.response_values)

    def respond_error(self, e):
        log.debug('RESPOND_ERROR %s %s' % (str(e), str(self.response_values)))
        return error_handler(e, **self.response_values)

    def process_request(self):
        """
        Этот метод надо переопределять в потомках
        """
        raise NotImplementedError()  # pragma: no cover

    @classmethod
    def as_view(cls, name=None, *args, **kwargs):
        name = name or cls.__name__
        return super(BaseView, cls).as_view(name, *args, **kwargs)

    def process_form(self, form, values):
        try:
            result = form.to_python(values)
            self.form_values = {key: value for key, value in result.items() if value is not None}
        except Invalid as e:
            _, self.form_values, _, error_list, error_dict = e.args
            assert error_list is None
            if error_dict:
                for error_field in error_dict:
                    if error_field in self.form_values:
                        del self.form_values[error_field]
            print(error_dict)
            raise InvalidRequestError(str(e))

    def check_no_repeated_params(self):
        for key in self.all_values:
            key_in_path_count = 1 if key in self.path_values else 0
            if len(request.args.getlist(key)) + len(request.form.getlist(key)) + key_in_path_count > 1:
                raise InvalidRequestError('Repeated parameter: %s' % key)

    def check_headers(self):
        names = [
            header.name for header in self.expected_headers
            if (header.name not in self.headers
                or (not self.headers.get(header.name) and not header.allow_empty_value))
        ]

        if names:
            print('Missed headers: {headers_names}'.format(headers_names='; '.join(names)))
            raise HeadersMissedError('Missed headers: {headers_names}'.format(headers_names='; '.join(names)))
