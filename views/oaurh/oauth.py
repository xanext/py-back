# coding=utf-8
import logging

from formencode import validators

from backend.oauth_backend import (
    get_account_if_has_access,
    generate_oauth_token,
)
from views.base import Schema, BaseView

log = logging.getLogger(__name__)


class OAuthForm(Schema):
    login = validators.String(not_empty=True, strip=True)
    password = validators.String(not_empty=True, strip=True)


class OAuth(BaseView):
    form = OAuthForm

    def process_request(self):
        login = self.form_values['login']
        password = self.form_values['password']
        account = get_account_if_has_access(login, password)

        token = generate_oauth_token(account)

        account_data = {
            'login': account.login,
            'firstname': account.firstname,
            'lastname': account.lastname,
            'age': account.age,
            'uid': account.uid
        }

        self.response_values = {
            'status': 'ok',
            'access_token': token,
            'account': account_data
        }


__all__ = (
    'OAuth',
)
