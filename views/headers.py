from collections import namedtuple

Header = namedtuple('Header', ['name', 'code_for_error', 'allow_empty_value'])

HEADER_CONSUMER_AUTHORIZATION = Header(
    name='Authorization',
    code_for_error='authorization',
    allow_empty_value=False,
)

__all__ = (
    'HEADER_CONSUMER_AUTHORIZATION',
)
