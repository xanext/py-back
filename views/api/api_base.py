import logging

from backend.oauth_backend import validate_oauth_token
from exceptions import AuthorizationHeaderError
from utils.decorators import cached_property
from views.base import BaseView
from views.headers import HEADER_CONSUMER_AUTHORIZATION

log = logging.getLogger(__name__)

OAUTH_HEADER_PREFIX = 'oauth '


class ApiBaseView(BaseView):

    @property
    def authorization(self):
        return self.headers.get(HEADER_CONSUMER_AUTHORIZATION.name)

    @cached_property
    def oauth_token(self):
        auth_header = self.authorization
        if not auth_header.lower().startswith(OAUTH_HEADER_PREFIX):
            raise AuthorizationHeaderError(
                'Authorization header value should be formatted like \'OAuth TOKEN_VALUE\''
            )
        return auth_header[len(OAUTH_HEADER_PREFIX):].strip()

    expected_headers = [HEADER_CONSUMER_AUTHORIZATION]

    def check_headers(self):
        super(ApiBaseView, self).check_headers()

        validate_oauth_token(self.oauth_token)
