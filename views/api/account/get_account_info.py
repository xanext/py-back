# coding=utf-8
import logging

from formencode import validators

from backend.oauth_backend import (
    get_account_by_uid)
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class GetAccountForm(Schema):
    uid = validators.Int(not_empty=True, strip=True)


class GetAccount(ApiBaseView):
    form = GetAccountForm

    def process_request(self):
        account = get_account_by_uid(self.form_values['uid'])
        account_data = vars(account)
        del account_data['password']
        del account_data['login']

        self.response_values = {
            'status': 'ok',
            'account_info': account_data,
        }


__all__ = (
    'GetAccount',
)
