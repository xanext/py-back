# coding=utf-8
import logging

from formencode import validators

from backend.track_backend import get_tracks_ids_by_point_id
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class GetTracksIdsForm(Schema):
    point_id = validators.Int(not_empty=True, strip=True)


class GetTracksIds(ApiBaseView):
    form = GetTracksIdsForm

    def process_request(self):
        tracks = get_tracks_ids_by_point_id(self.form_values['point_id'])
        self.response_values = {
            'status': 'ok',
            'tracks': tracks,
        }


__all__ = (
    'GetTracksIds',
)
