# coding=utf-8
import logging

from formencode import validators

from backend.track_backend import get_track_by_track_id
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class GetTrackForm(Schema):
    track_id = validators.Int(not_empty=True, strip=True)


class GetTrack(ApiBaseView):
    form = GetTrackForm

    def process_request(self):
        tracks = get_track_by_track_id(self.form_values['track_id'])
        self.response_values = {
            'status': 'ok',
            'track_id': self.form_values['track_id'],
            'track': tracks,
        }


__all__ = (
    'GetTrack',
)
