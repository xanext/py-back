# coding=utf-8
import logging

from formencode import validators

import validators as validator
from backend.track_backend import add_track
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class AddTrackForm(Schema):
    point_id = validators.Int(not_empty=True, strip=True)
    points = validator.PointValidator(not_empty=True, strip=True)


class AddTrack(ApiBaseView):
    form = AddTrackForm

    def process_request(self):
        track_id = add_track(self.form_values['point_id'], self.form_values['points']['points'])
        self.response_values = {
            'status': 'ok',
            'msg': 'Track successfully added',
            'track_id': track_id,
        }


__all__ = (
    'AddTrack',
)
