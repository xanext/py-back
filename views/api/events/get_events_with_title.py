# coding=utf-8
import logging

from formencode import validators

from backend.events_backend import (
    get_events_with_title,
)
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class GetEventsWithTitleForm(Schema):
    title = validators.String(not_empty=True, strip=True)


class GetEventsWithTitle(ApiBaseView):
    form = GetEventsWithTitleForm

    def process_request(self):
        self.response_values = {
            'status': 'ok',
            'events': get_events_with_title(self.form_values['title'])
        }


__all__ = (
    'GetEventsWithTitle',
)
