# coding=utf-8
import logging

from formencode import validators

from backend.events_backend import (
    get_events_with_offset
)
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class GetEventsForm(Schema):
    offset = validators.Int(not_empty=True, if_missing=0, strip=True)


class GetEvents(ApiBaseView):
    form = GetEventsForm

    def process_request(self):
        offset = self.form_values['offset']
        events = get_events_with_offset(offset),
        self.response_values = {
            'status': 'ok',
            'events': events,
        }
        if len(events) == 100:
            self.response_values['link'] = '/1/get_events/?offset={next_offset}'.format(next_offset=offset + 100)


__all__ = (
    'GetEvents',
)
