# coding=utf-8
import logging
from datetime import datetime

from formencode import validators

from backend.events_backend import add_event
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class AddEventForm(Schema):
    title = validators.String(not_empty=True, strip=True)
    description = validators.String(not_empty=True, strip=True)
    date = validators.String(not_empty=True, strip=True)
    author_uid = validators.Int(not_empty=True, strip=True)
    point_id = validators.Int(not_empty=True, strip=True)


class AddEvent(ApiBaseView):
    form = AddEventForm

    def process_request(self):
        title = self.form_values['title']
        description = self.form_values['description']
        date = datetime.strptime(self.form_values['date'], '%Y-%m-%d %H:%M:%S')
        author_uid = self.form_values['author_uid']
        point_id = self.form_values['point_id']

        event_id = add_event(title, description, date, author_uid, point_id)
        self.response_values = {
            'status': 'ok',
            'event_id': event_id,
            'msg': 'Event successfully added',
        }


__all__ = (
    'AddEvent',
)
