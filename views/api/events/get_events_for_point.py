# coding=utf-8
import logging

from formencode import validators

from backend.events_backend import (
    get_events_for_point,
)
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class GetEventsForPointForm(Schema):
    point_id = validators.Int(not_empty=True, strip=True)


class GetEventsForPoint(ApiBaseView):
    form = GetEventsForPointForm

    def process_request(self):
        self.response_values = {
            'status': 'ok',
            'events': get_events_for_point(self.form_values['point_id'])
        }


__all__ = (
    'GetEventsForPoint',
)
