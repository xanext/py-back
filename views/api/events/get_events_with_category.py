# coding=utf-8
import logging

from formencode import validators

from backend.events_backend import (
    get_events_with_category,
)
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class GetEventsWithCategoryForm(Schema):
    category = validators.String(not_empty=True, strip=True)


class GetEventsWithCategory(ApiBaseView):
    form = GetEventsWithCategoryForm

    def process_request(self):
        self.response_values = {
            'status': 'ok',
            'events': get_events_with_category(self.form_values['category'])
        }


__all__ = (
    'GetEventsWithCategory',
)
