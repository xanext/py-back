# coding=utf-8
import logging

from formencode import validators

from backend.events_backend import (
    get_events_by_uid)
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class GetEventsByUidForm(Schema):
    uid = validators.Int(not_empty=True, strip=True)


class GetEventsByUid(ApiBaseView):
    form = GetEventsByUidForm

    def process_request(self):
        self.response_values = {
            'status': 'ok',
            'events': get_events_by_uid(self.form_values['uid'])
        }


__all__ = (
    'GetEventsByUid',
)
