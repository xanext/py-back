# coding=utf-8
import logging

from backend.events_backend import (
    get_all_events
)
from views.api.api_base import ApiBaseView

log = logging.getLogger(__name__)


class GetAllEvents(ApiBaseView):

    def process_request(self):
        self.response_values = {
            'status': 'ok',
            'events': get_all_events()
        }


__all__ = (
    'GetAllEvents',
)
