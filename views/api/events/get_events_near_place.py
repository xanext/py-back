# coding=utf-8
import logging

from formencode import validators

from backend.events_backend import (
    get_events_near_place
)
from model.point import Point
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class GetEventsNearPlaceForm(Schema):
    point_x = validators.Number(not_empty=True, strip=True)
    point_y = validators.Number(not_empty=True, strip=True)
    scale = validators.Int(not_empty=True, if_missinhg=100, min=100, max=10000)


class GetEventsNearPlace(ApiBaseView):
    form = GetEventsNearPlaceForm

    def process_request(self):
        point = Point(self.form_values['point_x'], self.form_values['point_y'])
        self.response_values = {
            'status': 'ok',
            'events': get_events_near_place(point, self.form_values['scale'])
        }


__all__ = (
    'GetEventsNearPlace',
)
