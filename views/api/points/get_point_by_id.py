# coding=utf-8
import logging

from formencode import validators

from backend.point_backend import get_points_near_place, get_point_by_id
from model.point import Point
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class GetPointsByIdForm(Schema):
    point_id = validators.Int(not_empty=True, strip=True)


class GetPointById(ApiBaseView):
    form = GetPointsByIdForm

    def process_request(self):
        self.response_values = {
            'status': 'ok',
            'point': get_point_by_id(self.form_values['point_id'])
        }


__all__ = (
    'GetPointById',
)
