# coding=utf-8
import logging

from formencode import validators

from backend.point_backend import get_points_with_category
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class GetPointsWithCategoryForm(Schema):
    category = validators.String(not_empty=True, strip=True)


class GetPointsWithCategory(ApiBaseView):
    form = GetPointsWithCategoryForm

    def process_request(self):
        self.response_values = {
            'status': 'ok',
            'points': get_points_with_category(self.form_values['category'])
        }


__all__ = (
    'GetPointsWithCategory',
)
