# coding=utf-8
import logging

from backend.point_backend import get_all_points
from views.api.api_base import ApiBaseView

log = logging.getLogger(__name__)


class GetAllPoints(ApiBaseView):

    def process_request(self):
        self.response_values = {
            'status': 'ok',
            'points': get_all_points()
        }


__all__ = (
    'GetAllPoints',
)
