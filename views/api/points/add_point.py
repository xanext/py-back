# coding=utf-8
import logging

from formencode import validators

import validators as validator
from backend.point_backend import add_point
from views.api.api_base import ApiBaseView
from views.base import Schema

log = logging.getLogger(__name__)


class AddPointForm(Schema):
    category = validator.CategoryValidator(not_empty=True, strip=True)
    name = validators.String(not_empty=True, strip=True)
    point_x = validators.Number(not_empty=True, strip=True)
    point_y = validators.Number(not_empty=True, strip=True)
    point_z = validators.Number(not_empty=True, strip=True)


class AddPoint(ApiBaseView):
    form = AddPointForm

    def process_request(self):
        name = self.form_values['name']
        category = self.form_values['category']
        point_x = "{:12.8f}".format(self.form_values['point_x'])
        point_y = "{:12.8f}".format(self.form_values['point_y'])
        point_z = "{:12.8f}".format(self.form_values['point_z'])

        point_id = add_point(name, point_x, point_y, point_z, category)
        self.response_values = {
            'status': 'ok',
            'msg': 'Point successfully added',
            'point_id': point_id
        }


__all__ = (
    'AddPoint',
)
