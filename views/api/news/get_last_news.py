# coding=utf-8
import logging

from backend.events_backend import (
    get_events_in_last_10,
)
from views.api.api_base import ApiBaseView

log = logging.getLogger(__name__)


class GetLastNews(ApiBaseView):
    def process_request(self):
        events = get_events_in_last_10()
        tracks = []
        self.response_values = {
            'status': 'ok',
            'events': events,
            'tracks': tracks
        }


__all__ = (
    'GetLastNews',
)
