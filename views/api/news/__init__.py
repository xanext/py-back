from .get_last_news import GetLastNews

__all__ = [
    'GetLastNews'
]