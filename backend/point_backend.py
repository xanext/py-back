from database.db import get_db
from database.db_schema import (
    point_table,
)
from exceptions import AddPointError


def _build_points_response(point_list):
    return [{
        'point_id': point['point_id'],
        'name': point['name'],
        'category': point['category'],
        'point_x': str(point['point_x']),
        'point_y': str(point['point_y']),
        'point_z': str(point['point_z']),
    } for point in point_list]


def get_point_by_id(point_id):
    point_list = get_db().get(point_table, point_id=point_id)
    return _build_points_response(point_list)[0]


def get_all_points():
    point_list = get_db().get(point_table)
    return _build_points_response(point_list)


def get_points_near_place(point, scale):
    point_list = get_db().run_queries(get_db().get_points_near_point_query(point, scale))[0]
    return _build_points_response(point_list)


def get_points_with_category(category):
    point_list = get_db().get(point_table, category=category)
    return _build_points_response(point_list)


def add_point(name, point_x, point_y, point_z, category):
    point_list = get_db().get(
        point_table, name=name, point_x=point_x, point_y=point_y, point_z=point_z, category=category
    )
    if len(point_list) > 0:
        raise AddPointError('Point with same coordinates already exist')
    get_db().add(point_table, name=name, point_x=point_x, point_y=point_y, point_z=point_z, category=category)
    point_id = get_db().get(
        point_table, name=name, point_x=point_x, point_y=point_y, point_z=point_z, category=category
    )[0]['point_id']
    return point_id
