import random
import string

from werkzeug.security import check_password_hash, generate_password_hash

from database.db import get_db
from database.db_schema import (
    account_table,
    token_table,
)
from exceptions import (
    AuthorizationError,
    RegistrationError,
    OAuthTokenInvalid)
from model.account import Account


def __generate_token(n_digits):
    return 'AIODS' + ''.join(random.choice(string.ascii_letters + string.digits + '-') for _ in range(n_digits))


def __generate_uid():
    return int(''.join(random.choice(string.digits) for _ in range(12)))


def generate_oauth_token(account):
    token = __generate_token(55)
    get_db().remove(token_table, uid=account.uid)
    get_db().add(token_table, uid=account.uid, token=token)
    return token


def get_account_if_has_access(login, password):
    user_list = get_db().get(account_table, login=login)
    assert len(user_list) < 2, 'We have %d users with the same login=%s. WTF?' % (len(user_list), login)
    account = Account(**user_list[0]) if len(user_list) == 1 else None
    if not account:
        raise AuthorizationError('Account with login {login} doesn\'t exist'.format(login=login))
    has_access = check_password_hash(account.password, password)
    if has_access:
        return account
    else:
        raise AuthorizationError('Access denied. Wrong password')


def register_account(account):
    user_list = get_db().get(account_table, login=account.login)
    if len(user_list) > 0:
        raise RegistrationError('Account with same login already exist')
    account.uid = __generate_uid()
    account.password = generate_password_hash(account.password)
    get_db().add(account_table, uid=account.uid, login=account.login,
                 password=account.password, firstname=account.firstname,
                 lastname=account.lastname, age=account.age)
    return account


def validate_oauth_token(token):
    uids = get_db().get(token_table, token=token)
    assert len(uids) < 2, 'We have %d users with the same token. WTF?' % (len(uids))
    if len(uids) == 0:
        raise OAuthTokenInvalid('OAuth-token not valid. Please log in before make requests')


def get_account_by_uid(uid):
    user_list = get_db().get(account_table, uid=uid)
    assert len(user_list) < 2, 'We have %d users with the same uid=%s. WTF?' % (len(user_list), uid)
    return Account(**user_list[0]) if len(user_list) == 1 else None
