from database.db import get_db
from database.db_schema import event_table, point_table
from exceptions import AddEventError, PointIdInvalid


def _build_events_response(event_list):
    return [{
        'event_id': event['event_id'],
        'title': event['title'],
        'description': event['description'],
        'point': {
            'point_id': event['point_id'],
            'name': event['name'],
            'category': event['category'],
            'point_x': str(event['point_x']),
            'point_y': str(event['point_y']),
            'point_z': str(event['point_z']),
        },
        'date': str(event['date']),
        'author_uid': event['author_uid'],
    } for event in event_list]


def _build_events_response_with_point(event_list, point):
    return [{
        'event_id': event['event_id'],
        'title': event['title'],
        'description': event['description'],
        'point': {
            'point_id': point['point_id'],
            'name': point['name'],
            'category': point['category'],
            'point_x': str(point['point_x']),
            'point_y': str(point['point_y']),
            'point_z': str(point['point_z']),
        },
        'date': str(event['date']),
        'author_uid': event['author_uid'],
    } for event in event_list]


def get_all_events():
    event_list = get_db().run_queries(get_db().get_all_events_query())[0]
    return _build_events_response(event_list)


def get_events_with_offset(offset):
    event_list = get_db().run_queries(get_db().get_events_with_offset_query(offset))[0]
    return _build_events_response(event_list)


def get_events_with_title(title):
    event_list = get_db().run_queries(get_db().get_events_with_title_query(title))[0]
    return _build_events_response(event_list)


def get_events_with_category(category):
    event_list = get_db().run_queries(get_db().get_events_with_category_query(category))[0]
    return _build_events_response(event_list)


def get_events_by_uid(uid):
    event_list = get_db().run_queries(get_db().get_events_by_uid_query(uid))[0]
    return _build_events_response(event_list)


def get_events_near_place(point, scale):
    event_list = get_db().run_queries(get_db().get_events_near_point_query(point, scale))[0]
    return _build_events_response(event_list)


def get_events_for_point(point_id):
    point_list = get_db().get(point_table, point_id=point_id)
    if len(point_list) == 0:
        raise PointIdInvalid('Point with id {point_id} doesn\'t exist'.format(point_id=point_id))
    point = point_list[0]
    event_list = get_db().run_queries(get_db().get_events_for_point_query(point_id))[0]
    return _build_events_response_with_point(event_list, point)


def add_event(title, description, date, author_uid, point_id):
    event_list = get_db().get(
        event_table, point_id=point_id, title=title, description=description, date=date, author_uid=author_uid
    )
    if len(event_list) > 0:
        raise AddEventError('Same event already exist')
    get_db().add(
        event_table, point_id=point_id, title=title, description=description, date=date, author_uid=author_uid
    )
    event_id = get_db().get(
        event_table, point_id=point_id, title=title, description=description, date=date, author_uid=author_uid
    )[0]['event_id']
    return event_id


def get_events_in_last_10():
    event_list = get_db().run_queries(get_db().get_events_in_last_10_query())[0]
    return _build_events_response(event_list)
