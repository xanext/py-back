import random
import string

from database.db import get_db
from database.db_schema import (
    track_table
)


def __generate_track_id():
    return int(''.join(random.choice(string.digits) for _ in range(12)))


def _build_track_response(tracks):
    return [{
        'point_number': track['point_number'],
        'point_x': str(track['point_x']),
        'point_y': str(track['point_y']),
        'point_z': str(track['point_z']),
    } for track in tracks]


def add_track(point_id, points):
    track_id = __generate_track_id()
    for point in points:
        point_x = float(point['point_x'])
        point_y = float(point['point_y'])
        point_z = float(point['point_z'])
        print(float(point['point_x']))
        get_db().add(track_table, track_id=track_id, point_id=point_id, point_x=point_x, point_y=point_y,
                     point_z=point_z)
    return track_id


def get_track_by_track_id(track_id):
    track = get_db().run_queries(get_db().get_track_by_track_id(track_id))[0]
    return _build_track_response(track)


def get_tracks_ids_by_point_id(point_id):
    tracks = get_db().run_queries(get_db().get_track_by_point_id(point_id))[0]
    return [{'track_id': track['track_id']} for track in tracks]
