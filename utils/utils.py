# -*- coding: utf-8 -*-
import json
import logging
import random
import string
import sys
import urllib.parse
from functools import wraps

from flask import (
    make_response,
)

from exceptions import XanextServiceException, TemporarilyUnavailableError

ESCAPABLE_URL_CHARACTERS = {c: urllib.parse.quote(c) for c in {'\n', '\r', ' '}}

log = logging.getLogger(__name__)


def random_numeric(n_digits):
    return ''.join(random.choice(string.digits) for _ in range(n_digits))


def random_alphanumeric(n_digits):
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(n_digits))


def retry_decorator(attempts=3, exception=Exception):
    if type(exception) == list:
        exception = tuple(exception)
    elif type(exception) != tuple:
        assert type(exception) == type, 'exception should be subclass of Exception'
        assert issubclass(exception, Exception), 'exception should be subclass of Exception'

    def decorator(f):
        @wraps(f)
        def _wrapper(*args, **kwargs):
            for i in range(attempts - 1):
                try:
                    return f(*args, **kwargs)
                except exception as e:
                    log.debug('Retry(%d) %s after: %s' % (i + 1, f.__name__, str(e)))
            return f(*args, **kwargs)

        return _wrapper

    return decorator


def rename_exception_decorator(exception, new_exception):
    def _decorator(f):
        @wraps(f)
        def _wrapper(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except exception as e:
                raise new_exception(str(e))(None).with_traceback(sys.exc_info()[2])

        return _wrapper

    return _decorator


def escape_query_characters_middleware(wsgi_app):
    """
    Экранирует символы в HTTP-Query.
    """

    @wraps(wsgi_app)
    def wrapper(environ, start_response):
        qs = environ.get('QUERY_STRING')
        if qs:
            new_qs = qs
            for esc_ch, repl in ESCAPABLE_URL_CHARACTERS.items():
                new_qs = new_qs.replace(esc_ch, repl)
            if new_qs != qs:
                log.debug(u'Escape CR/LF characters in query')
            environ['QUERY_STRING'] = new_qs
        return wsgi_app(environ, start_response)

    return wrapper


def json_response(data, return_code):
    log.info('Return code %d\tdata: %s' % (return_code, data))
    return make_response(
        json.dumps(data, indent=4, sort_keys=True) + '\n',
        return_code,
        {'Content-Type': 'application/json'},
    )


def ok_response(**kwargs):
    return json_response(kwargs, 200)


def error_response(**kwargs):
    return json_response(kwargs, 400)


def get_exception_dict(e):
    if isinstance(e, XanextServiceException):
        description = str(e)
        if e.error_description:
            description = e.error_description
        return dict(
            error=e.code,
            error_description=description,
        )
    return dict(error='server_error', error_description='%s: %s' % (type(e).__name__, str(e)))


def error_handler(e, **kwargs):
    response_data = dict(get_exception_dict(e), **kwargs)
    print('--------ERROR---------')
    print(response_data)
    print('----------------------')
    if isinstance(e, XanextServiceException):
        log.debug('processing exception', exc_info=True)
        if isinstance(e, TemporarilyUnavailableError):
            return json_response(response_data, 503)
        return error_response(**response_data)
    log.exception('exception.unhandled')
    return json_response(response_data, 500)
