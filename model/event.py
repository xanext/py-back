import json


class Event:
    def __init__(self, event_id, title, description, point_id, date, author_uid):
        self.event_id = event_id
        self.title = title
        self.description = description
        self.point_id = point_id
        self.date = date
        self.author_uid = author_uid

    def __repr__(self):
        return json.dumps(self.__dict__)
