import json


class Account:
    def __init__(self, uid, login, password, firstname, lastname, age):
        self.uid = uid
        self.login = login
        self.password = password
        self.firstname = firstname
        self.lastname = lastname
        self.age = age

    def __repr__(self):
        return json.dumps(self.__dict__)
