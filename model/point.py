import json


class Point:
    def __init__(self, point_x, point_y, point_z = 0):
        self.point_x = point_x
        self.point_y = point_y
        self.point_z = point_z

    def __repr__(self):
        return json.dumps(self.__dict__)


class DBPoint:
    def __init__(self, point_x, point_y, point_z, category, point_id=0):
        self.point_x = point_x
        self.point_y = point_y
        self.point_z = point_z
        self.point_id = point_id
        self.category = category

    def __repr__(self):
        return json.dumps(self.__dict__)
