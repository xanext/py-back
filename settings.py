point_categories = (
    'Playground',
    'Bar',
    'Sport box',
    'Stadium',
    'Gym',
    'Park',
    'Track',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'local': {
            'format': '%(asctime)s %(levelname)s %(name)s:%(lineno)d %(message)s',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
            'formatter': 'local',
            'stream': 'ext://sys.stdout',
        },
    }
}
